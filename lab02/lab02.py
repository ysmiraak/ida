from sklearn.datasets import load_iris

data = load_iris()
X = data.data
y = data.target

import matplotlib.pyplot as plt
import numpy as np

# 1


def naive_decision_rule(x, sepal_length= 0, sepal_width= 1, petal_length= 3, petal_width= 4, setosa= 0, versicolor= 1, virginica= 2):
    if x[sepal_length] - x[sepal_width] <= 2.2:
        return setosa
    else:
        return versicolor


def predict(model, X):
    """Builds prediction on a matrix X given a model for each data point in a row.
    Returns a flat vector of predictions.
    """
    return np.apply_along_axis(model, axis=1, arr=X)


# 2


def zero_one_loss(y_pred, y_true):
    return np.sum(y_pred != y_true)


# 3


for c in range(3):
    x = X[y == c, 3]
    x.sort()
    print("\n", x)


def my_decision_rule(x, sepal_length= 0, sepal_width= 1, petal_length= 3, petal_width= 4, setosa= 0, versicolor= 1, virginica= 2):
    pl = x[petal_length]
    if pl <= 0.8:
        return setosa
    elif pl <= 1.65:
        return versicolor
    else:
        return virginica
