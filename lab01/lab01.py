import pandas as pd
import numpy as np

from pylab import rcParams
rcParams['figure.figsize'] = 8, 5
import seaborn as sns

# 1

dat = pd.read_csv("telecom_churn.csv")
dat.head(5)

# 2

dat.shape
dat.info()
dat.describe()

# 3

dat['Churn'] = dat['Churn'].astype('int64')
sns.countplot(dat['Churn'])

# 4

from itertools import product

for o1, o2 in product((True, False), repeat= 2):
    tmp = dat.sort_values(
        ['Churn', 'Total eve calls', 'Total day charge']
        , ascending= [o2, o2, o1])
del o1, o2, tmp

# 5

dat.loc[:,'Churn'].mean()
dat.groupby('Churn').mean()
dat.groupby('Churn').mean().loc[1,'Total day minutes']
dat.groupby(['Churn', 'International plan']).max().loc[0,'No']['Total intl minutes']

# 6

dat.hist()

# 7

pd.crosstab(dat['International plan'], dat['Churn'])


import matplotlib.pyplot as plt

feats = 'International plan', 'Voice mail plan', 'Customer service calls'
f, axarr = plt.subplots(len(feats), 1, figsize= (8, 18))
for feat, ax in zip(feats, axarr):
    xtab = pd.crosstab(dat[feat], dat['Churn'])
    xtab.plot(kind= 'bar', ax= ax)
del feats, f, axarr, feat, ax, xtab

# 8

feat = 'Over 3 service calls'
dat[feat] = dat['Customer service calls'] > 3
pd.crosstab(dat[feat], dat['Churn']).plot(kind= 'bar')
del feat
