# coding: utf-8

# # Evaluation
#
# The goal of this lab is to introduce you to techniques for
# evaluating your trained models. The motivation is to be able to
# select the model that has the best (expected) out-of-sample
# prediction and to assess the quality of the model.
#
# ## 1. Model Selection in a holdout setting
#
# We will work with the <a
# href="https://en.wikipedia.org/wiki/Iris_flower_data_set">Iris</a>
# data set. The iris data set consists out of $4$ features (sepal
# length, sepal width, petal length, petal width) of three kinds of
# flowers in the iris family: iris setosa, iris versicolor, iris
# virginica. Our version of the data set has 150 data points with 50
# for each class.

# In[ ]:

import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
iris = load_iris()
print('Loaded {} data points'.format(len(iris.data)))
X, y = iris.data, iris.target
print('Class labels: {}'.format(zip(range(3), iris.target_names)))

# We try to classify the iris versicolor with the help of the first two features.

# In[ ]:

import numpy as np
X_versi = X[:, :2]
y_versi = np.zeros(len(y))
y_versi[y == 1] = 1

# In[ ]:

# plot iris data with two features
plt.scatter(X_versi[y_versi == 1, 0], X_versi[y_versi == 1, 1], c='red', label='iris versicolor')
plt.scatter(X_versi[y_versi == 0, 0], X_versi[y_versi == 0, 1], c='green', label='other')
plt.xlabel("x1")
plt.ylabel("x2")
plt.title('Iris data')
plt.legend()
plt.show()

# We split the data into a train and test (holdout) set with a split ratio of 75% to 25%.

# In[ ]:

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X_versi, y_versi, test_size=0.25, random_state=3)

# The following function is a little visualization helper that draws
# the values of the decision function on a heat map given a matplotlib
# axe.

# In[ ]:

def show_decision_function(clf, ax):
    xx, yy = np.meshgrid(np.linspace(4.5, 8, 200), np.linspace(1.5, 4.0, 200))
    try:
        Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
    except AttributeError:
        Z = clf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 0]
    Z = Z.reshape(xx.shape)
    ax.pcolormesh(xx, yy, Z, cmap=plt.cm.jet)
    ax.set_xlim(4.5, 8)
    ax.set_ylim(1.5, 4.0)
    ax.set_xticks(())
    ax.set_yticks(())
    ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, s=100)

# We now train a SVM classifier on the training data and plot its decision boundary.

# In[ ]:

from sklearn.svm import SVC
clf_svm = SVC(gamma=10, C=1)
clf_svm.fit(X_train, y_train)
fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(111)
show_decision_function(clf_svm, ax)
ax.set_title('Decision function of a SVM classifier with gamma = 10, C = 1')
plt.show()

# #### Exercise 1.1 (Performance measures)
#
# Classify the test data and evaluate the classification performance
# of the trained model 'clf_svm' using the scikit-learn metrics
# package. Compare various metrics (classification accuracy,
# precision, recall, f-score), interpret their values and argue which
# of them might be the most meaningful to report.

# In[ ]:

from sklearn import metrics

p = clf_svm.predict(X_test)

print("accuracy", metrics.accuracy_score(y_test, p))
print("precision", metrics.precision_score(y_test, p))
print("recall", metrics.recall_score(y_test, p))
print("f1", metrics.f1_score(y_test, p))

print("class ratio", y_train.sum() / y_train.size)

# since the classes are unbalanced, about 1/3 vs 2/3, the f1 score is
# the most meaningful here.

# #### Exercise 1.2 (ROC curve)
#
# To further evaluate the model, we want to plot a ROC (Receiver
# operating characteristic) curve and look at the AUC (area under the
# curve) value. Therefore, we provide a function "plot_roc_curves"
# that accepts increasing false positive rates (fprs) and true
# positive rates (tprs) and plots the curve for you.
#
# Please calculate the false and true positive rates of the classifier
# 'clf_svm' on the test data 'X_test' and draw the ROC curve by
# executing the function 'plot_roc_curves'. What does the ROC and AUC
# tell us about the classifier's performance?

# In[ ]:

# helper to plot ROC curves
def plot_roc_curves(fprs, tprs):
    fig = plt.figure(figsize=(20,10))
    for fpr, tpr in zip(fprs, tprs):
        plt.plot(fpr, tpr, label='ROC curve (AUC = %0.2f)' % metrics.auc(fpr, tpr))
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.show()

# In[ ]:

fpr, tpr, _ = metrics.roc_curve(y_test, p)

# plot the curve
plot_roc_curves([fpr], [tpr])
plt.show()

# auroc closer the 1.0 the better, closer to 0.5 the worse.
# 0.69 is not very good.

# #### Exercise 1.3 (Model comparison)
#
# Train four more SVM models on the training data by varying the
# regularization parameter $C$ (the gamma parameter can be set to 10
# again). Put the models into a list 'clfs' using the append
# method. You can add a name to each classifier by setting clf.name =
# "some description" to keep track of the configuration.

# In[ ]:

clfs = [SVC(gamma= 10, C= cost).fit(X_train, y_train) for cost in (1e-2, 1e-1, 1e1, 1e2)]

# Let's have a look at the decision functions of the four classifiers...

# In[ ]:

# visualize the decision functions of the four different classifiers
fig, axes = plt.subplots(2, 2, figsize=(20, 10))
for clf, ax in zip(clfs, axes.ravel()):
    show_decision_function(clf, ax)
    ax.set_title(clf.C)
plt.show()

# In[ ]:

# draw the roc curves:
fprs, tprs = [], []
for clf in clfs:
    y_score = clf.decision_function(X_test)
    fpr, tpr, _ = metrics.roc_curve(y_test, y_score, pos_label=1)
    fprs.append(fpr)
    tprs.append(tpr)
plot_roc_curves(fprs, tprs)
plt.show()

# ## 2. Hyperparameter Tuning
#
# Many models have hyperparameters, parameters that can't directly be
# estimated from the data. They have to be manually tuned by the
# practioner, because there is no analytical formula available to
# calculate an appropriate value. One example is the regularization
# parameter $C$ in SVMs.
#
# #### Exercise 2.1 (Nested cross-validation)
#
# Train a SVM classifier for the detection of iris versicolor again,
# but this time with a proper tuning of the regularization parameter
# $C$ (you may set the gamma parameter to 10 again). Select a
# reasonable range of parameter values for $C$ and implement a nested
# cross-validation (as shown on the slides) by yourself.

# You can use the following helper function that creates a list of
# masks. Each mask can be used as an index set to select the test
# samples. The function accepts the number of samples *num_samples* in
# the dataset and the desired number of folds *k* as input
# parameters. Since the data is sorted by the labels the k-fold CV
# will likely have trouble with class imbalances in the some cases. So
# you should randomly shuffle the data before applying the masks.

# In[ ]:

# helper function to create k-fold train-test-splits
def create_kfold_mask(num_samples, k):
    masks = []
    fold_size = num_samples // k
    for i in range(k):
        mask = np.zeros(num_samples, dtype=bool)
        mask[i*fold_size:(i+1)*fold_size] = True
        masks.append(mask)
    return masks

# visualization of the splits created by 'create_kfold_mask'
masks = create_kfold_mask(len(y_train), 10)
plt.matshow(masks)
plt.show()

# In[ ]:

np.random.seed(0)
np.random.shuffle(X_train)
np.random.seed(0)
np.random.shuffle(y_train)

costs = 2 ** np.arange(9)

f1 = np.array([
    np.mean([
        metrics.f1_score(
            y_train[mask]
            , SVC(gamma= 10, C= cost)
            .fit(X_train[~mask], y_train[~mask])
            .predict(X_train[mask]))
        for mask in masks])
    for cost in costs])

plt.plot(np.log(costs), f1)
plt.show()
