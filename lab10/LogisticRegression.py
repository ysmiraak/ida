# coding: utf-8

# ## Logistic regression
#
# In this lab you will get some hands-on experience with one of the
# most popular classification methods: logistic regression. In the
# problem of binary classification, it can be used to predict the
# probability that an instance $x_{i}$ belongs to a certain class
# (e.g. $+1$).

# ### Exercise 1
#
# In the first exercise we want you to implement a logistic regression
# classifier by yourself and compare this model to sklearn's
# implementation of logistic regression. First let's generate some
# data that is easily seperable.

# In[1]:

import numpy as np
import matplotlib.pyplot as plt

np.random.seed(1)
num_observations = 2000

x_class1 = np.random.multivariate_normal([0, 0], [[1, .25],[.25, 1]], num_observations)
x_class2 = np.random.multivariate_normal([1, 4], [[1, .25],[.25, 1]], num_observations)

# Training data:
X_train = np.vstack((x_class1, x_class2)).astype(np.float32)
y_train = np.hstack((np.zeros(num_observations), np.ones(num_observations)))

# If we plot the data, we see it is linearly seperable.

# In[2]:

plt.figure(figsize=(10,8))
plt.scatter(X_train[y_train == 1, 0], X_train[y_train == 1, 1], c='green', label='label 1')
plt.scatter(X_train[y_train == 0, 0], X_train[y_train == 0, 1], c='red', label='label 0')
plt.xlabel("x1")
plt.ylabel("x2")
plt.title('Sampled data')
plt.legend()
plt.show()

# #### Exercise 1.1 (Gradient descent) Let $\mathbf{x} \in
# \mathbb{R}^{d}$, $y \in \{0,1\}$ be some training example and
# $\mathbf{X} \in \mathbb{R}^{n\times d}$, $\bf{y} \in \mathbb{R}^{n}$
# be the training data set. In logistic regression the weights
# $\mathbf{w}$ of a simple linear model $\mathbf{w}^T\mathbf{x}$ are
# learned by minimizing the negative log likelihood of the training
# data. The likelihood can be written as:
# $$p(\mathbf{y}|\mathbf{X},\mathbf{w}) = \prod_{i|y_i = 1}
# \frac{\exp(\mathbf{w}^T\mathbf{x}_i)}{1+\exp(\mathbf{w}^T\mathbf{x}_i)}
# \prod_{i|y_i = 0} \frac{1}{1+\exp(\mathbf{w}^T\mathbf{x}_i)},$$ then
# we obtain the log likelihood $$\log
# p(\mathbf{y}|\mathbf{X},\mathbf{w}) = \sum_i y_i
# \mathbf{w}^T\mathbf{x}_i - \log(1 + \exp(\mathbf{w}^T
# \mathbf{x}_i))$$
#
# Since there is no analytic solution for this problem, we need to
# perform numeric optimization, e.g. gradient descent, in order to
# find the optimal weights $\mathbf{w}^* = \arg \min_{\mathbf{w}} -
# \log p(\mathbf{y}|\mathbf{X},\mathbf{w})$.
#
# Try to understand the following function logistic_regression(X, y,
# num_steps, learning_rate, add_intercept).  A few functions are
# missing by now: sigmoid(), log_likelihood(),
# log_likelihood_gradient(). These have to be implemented in the next
# exercises.

# In[ ]:


def predict(X, weights):
    if len(weights) == 1 + X.shape[1]: X = add_bias(X)
    return 0 < X @ weights


add_bias = lambda x: np.concatenate((np.ones_like(x[:,:1]), x), -1)
accuracy = lambda y, p: np.mean(y == p)
pred_acc = lambda X, y, weights: accuracy(y, predict(X, weights))


def logistic_regression(X, y, num_steps, learning_rate, add_intercept):
    if add_intercept: X = add_bias(X)
    weights = np.zeros(X.shape[1])
    for step in range(num_steps):
        weights -= learning_rate * log_likelihood_gradient(X, y, weights)
        if not step % 1000: print(log_likelihood(X, y, weights), pred_acc(X, y, weights))
    return weights

# #### Exercise 1.2 (Log likelihood)
#
# Write a function that calculates the negative log likelihood of the data.

# In[ ]:

def log_likelihood(X, y, weights):
    logit = X @ weights
    return np.sum(np.log1p(np.exp(logit)) - y * logit)

# #### Exercise 1.4 (Sigmoid function)
#
# A logistic regression classifier predicts the probability that an
# instance $\mathbf{x}$ belongs to a certain class $y=1$, by $p(y=1|
# \mathbf{x}, \mathbf{w}) = \sigma(\mathbf{x}^T\mathbf{w})$. It uses
# the sigmoid function $\sigma$ to map the outputs/scores of a linear
# model into probablities: $[-\infty, +\infty] \to [0,1]$.
#
# Please implement the sigmoid function, that accepts a vector of
# scores as input and returns a vector of probabilities.

# In[ ]:

def sigmoid(scores):
    return 1 / (1 + np.exp(- scores))

# #### Exercise 1.5 (Log likelihood gradient)
#
# Write a function that returns the gradient of the negative log
# likelihood. If we derive the log likelihood with respect to
# $\mathbf{w}$, we obtain the gradient as $$\mathbf{g} = \mathbf{X}^T
# (\mathbf{y} - \sigma(\mathbf{X}^T \mathbf{w})).$$

# In[1]:

def log_likelihood_gradient(X, y, weights):
    return (sigmoid(X @ weights) - y) @ X

# #### Exercise 1.6 (Learning) Train the weights of the logistic
# regression model on the training data, using the function
# logistic_regression(). Select a reasonable value for the number of
# steps (e.g. $20000$) and learning rate (e.g. $5e-5$) and make use of
# the option to add an intercept to the data.

# In[ ]:

weights = logistic_regression(X_train, y_train, 2000, 1e-3, True)

# #### Exercise 1.7 (sklearn)
#
# Now use the sklearn package LogisticRegression to train a logistic
# regression classifier clf on the same data and compare its weights
# to the weights of your model. sklearn uses L2 regularization by
# default, so you should turn it off to make results comparable.

# In[ ]:

from sklearn.linear_model import LogisticRegression

clf = LogisticRegression(C= 1e16).fit(X_train, y_train)

print(clf.intercept_, clf.coef_)
print(weights)

# #### Exercise 1.8 (Classification)
#
# Calculate predictions for the training data (X_train with added
# intercept) using your logistic regression model (i.e. calculate the
# scores of the linear model and map it to probabilities using the
# sigmoid function) and compare your results to sklearn's accuracy.

# In[ ]:

preds = predict(X_train, weights)

# Compare results:
print('Your accuracy: {0}'.format(accuracy(y_train, preds)))
print('Sklearn\'s accuracy: {0}'.format(clf.score(X_train, y_train)))

# ### Exercise 2
#
# Now we consider another dataset which consists of 22 features, each
# containing a test result for microchips. The target variable is
# whether the chip is "defect" or "not defect".

# First we load the dataset into a dataframe. We will only consider
# the first two features 'test1' and 'test2'.

# In[ ]:

import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LogisticRegression

data = pd.read_csv('data_chiptests.txt',header=None, names = ('test1','test2','released'))
data.info()

# In[ ]:

X = data.iloc[:,:2].values
y = data.iloc[:,2].values

plt.scatter(X[y == 1, 0], X[y == 1, 1], c='green', label='Non Defect')
plt.scatter(X[y == 0, 0], X[y == 0, 1], c='red', label='Defect')
plt.xlabel("test1")
plt.ylabel("test2")
plt.title('MicrochipTest')
plt.legend()
plt.show()

# #### Exercise 2.1 (sklearn)
#
# Use the sklearn package LogisticRegression again to implement a
# simple logistic regression classifier clf. Consider the cases, where
# the regularization parameter is chosen to be: $C=0.01;C=0.1; C=1;
# C=100$. In each case compute the accuracy on the training
# sample. What do you observe?

# In[ ]:

from sklearn.linear_model import LogisticRegression

for cost in 1e-2, 1e-1, 1e0, 1e2:
    clf = LogisticRegression(C= cost).fit(X, y)
    print(cost, clf.score(X, y), sep= "\t")

# 0.01	0.4322033898305085
# 0.1	0.5254237288135594
# 1.0	0.5423728813559322
# 100.0	0.5508474576271186

# about random chance.
# lower regularization is better.
# underfitting.

# We use a function plot_boundary to plot the decision boundary of the trained model.

# In[ ]:

def plot_boundary(clf, X, y, grid_step=.01, poly_featurizer=None):
    x_min, x_max = X[:, 0].min() - .1, X[:, 0].max() + .1
    y_min, y_max = X[:, 1].min() - .1, X[:, 1].max() + .1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, grid_step), np.arange(y_min, y_max, grid_step))
    if poly_featurizer == None:
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    else:
        Z = clf.predict(poly_featurizer.transform(np.c_[xx.ravel(), yy.ravel()]))
    Z = Z.reshape(xx.shape)
    plt.contour(xx, yy, Z, cmap = plt.cm.Paired)

# If everything went well, we can now have a look at the decision
# boundary of the trained model together with the data. What do you
# observe?

# In[ ]:

plot_boundary(clf, X, y, grid_step=.01)
plt.scatter(X[y == 1, 0], X[y == 1, 1], c='green', label='Non Defect')
plt.scatter(X[y == 0, 0], X[y == 0, 1], c='red', label='Defect')
plt.xlabel("Test1")
plt.ylabel("Test2")
plt.title('Logit with C= {}'.format(cost))
plt.legend()
plt.show()

print("accuracy:", round(clf.score(X, y), 3))

# #### Exercise 2.2 (Polynomial features)
#
# As you have seen the performance of simple logistic regression for
# linearly not separable data is quite poor. Polynomial features for
# 2-dimensional vector $x = (x_{1},x_{2})$ of dimension $r$ would be
# the set of features: $$\{x_{1}^{i}x_{2}^{j}\}_{i+j\leq r, 0 \leq i,j
# \leq r}.$$
#
# For example for $r=2$ the polynomial features for
# $x=\{x_{1},x_{2}\}$ would be:
# $$\{1,x_{1},x_{2},x_{1}x_{2},x_{1}^{2},x_{2}^{2}\}$$
#
# Compute polynomial features of degree $r=3$. Use the sklearn package
# PolynomialFeatures to produce polynomial features for the instances
# $\mathbb{X}$ (Hint: method "fit_transform" will be useful).

# In[ ]:

from sklearn.preprocessing import PolynomialFeatures

poly = PolynomialFeatures(degree= 3)
X_poly = poly.fit_transform(X)

# #### Exercise 2.3
#
# Now train a logistic regression model clf_2 using the polynomial features.

# In[ ]:

for cost in 1e-2, 1e-1, 1e0, 1e2:
    clf_2 = LogisticRegression(C= cost).fit(X_poly, y)
    print(cost, clf_2.score(X_poly, y), sep= "\t")

# 0.01	0.5338983050847458
# 0.1	0.7203389830508474
# 1.0	0.7966101694915254
# 100.0	0.847457627118644

# Let's evaluate and plot the decision boundary again. What do you observe?

# In[ ]:

plot_boundary(clf_2, X, y, grid_step=.01, poly_featurizer=poly)
plt.scatter(X[y == 1, 0], X[y == 1, 1], c='green', label='Not Defect')
plt.scatter(X[y == 0, 0], X[y == 0, 1], c='red', label='Defect')
plt.xlabel("Test1")
plt.ylabel("Test2")
plt.title('Logit with C= {}'.format(cost))
plt.legend()
plt.show()

print("Prediction Score:", round(clf_2.score(X_poly, y), 3))
