from collections import Counter
import numpy as np


normalize = lambda p: p / p.sum()


def xent(p, q= None):
    """returns the joint entropy of discrete distributions `p` and `q`,
    which are vectors of probabilities with matching components.

    when `q` is None, returns the entropy of `p`.

    """
    if q is None: q = p
    assert len(p) == len(q)
    return - np.dot(p, np.log2(q))


def jent(x, y= None):
    """returns the joint entropy of discrete variables `x` and `y`, which
    are vectors of values with matching components.

    when `y` is None, returns the entropy of `x`.

    """
    assert y is None or len(x) == len(y)
    z = x if y is None else zip(x, y)
    return xent(normalize(np.fromiter(Counter(z).values(), dtype= np.float)))


# H( y | x )
cent = lambda y, x: jent(x, y) - jent(x)
# H( p || q )
rent = lambda p, q: xent(p, q) - xent(p)
# I( x ; y)
minf = lambda x, y: jent(x) + jent(y) - jent(x, y)
