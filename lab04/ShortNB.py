# Short implementation for test&training errors of regression decision trees

import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt


def generate_wave_set(n_support=1000, n_train=25, std=0.3):

    support = np.linspace(0, 2*np.pi, num=n_support)
    values = np.sin(support)+1
    # choosing randomly points from the support
    X_train = np.sort(np.random.choice(support, size=n_train, replace=False)).reshape((-1,1))
    # calculate sin(x) + 1 and add the noise
    y_true = np.sin(X_train).reshape((-1,1)) + 1
    y_train = y_true + np.random.normal(0, std, size=X_train.shape[0]).reshape((-1,1))
    return X_train,y_train,support,values,y_true


# plot generation
from sklearn import tree

X,y,sup,val,y_true= generate_wave_set(100,25)
margin = 0.3
sup = sup.reshape((-1,1))
plt.rcParams['figure.figsize'] = (14,10)

plt.plot(sup, val, 'b--', alpha=0.75, label='curve')
plt.scatter(X, y, 40, 'r', 'o', alpha=0.9, label='data')

plt.xlim(X.min() - margin, X.max() + margin)
plt.ylim(y.min() - margin, y.max() + margin)
plt.title('True cruve and noised data')
plt.xlabel('x')
plt.ylabel('y')

print sup.shape

clf = tree.DecisionTreeRegressor(max_depth=10)
clf.fit(X,y)
y_hat = clf.predict(sup)
plt.plot(sup, y_hat, c="g", label="dec_tree_est")
plt.legend(loc='upper right', prop={'size': 20})

plt.show()


from numpy import linalg as LA
upper_bound = 50
error_train = np.zeros((upper_bound,1))
error_test = np.zeros((upper_bound,1))
for i in range(1,upper_bound):
    clf = tree.DecisionTreeRegressor(max_depth=i)
    clf.fit(X,y)
    y_hat_train = clf.predict(X)
    y_hat = clf.predict(sup)
    print 'Train error at depth',i, LA.norm(y-y_hat_train)/len(y)
    print 'Test error at depth',i, LA.norm(val-y_hat)/len(val)
